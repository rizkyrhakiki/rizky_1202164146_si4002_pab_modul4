package com.android.rizky_1202164146_si4002_pab_modul4;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class AdapterItem extends RecyclerView.Adapter<AdapterItem.ViewHolder>  {
    private ArrayList<Item> daftarItem;
    private Context mContext;

    public AdapterItem(ArrayList<Item> daftarChat, Context mContext) {
        this.daftarItem = daftarChat;
        this.mContext = mContext;

    }
    @Override
    public AdapterItem.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterItem.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_menu, viewGroup, false)) {
        };
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterItem.ViewHolder viewHolder, int position) {
        Item item = daftarItem.get(position);
        viewHolder.bindTo(item);
    }

    @Override
    public int getItemCount() {
        return daftarItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView nama, harga;
        private ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama_item);
            harga = itemView.findViewById(R.id.harga_item);
            image = itemView.findViewById(R.id.item_image);


            itemView.setOnClickListener(this);
        }

        public void bindTo(Item item) {
            nama.setText(item.getNama());
            harga.setText(item.getHarga());

            final StorageReference islandRef = FirebaseStorage.getInstance().getReference().child("images/" + item.getImage());

            final long ONE_MEGABYTE = 10* 1024 * 1024;
            islandRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Drawable d = Drawable.createFromStream(new ByteArrayInputStream(bytes), null);
                    image.setImageDrawable(d);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    image.setImageResource(R.drawable.ic_launcher_background);
                }
            });
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, nama.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
