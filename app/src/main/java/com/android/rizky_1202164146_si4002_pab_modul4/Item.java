package com.android.rizky_1202164146_si4002_pab_modul4;

public class Item {
    String image,nama,harga,dekripsi;

    public Item(String image, String nama, String harga, String deskripsi) {
        this.image = image;
        this.nama = nama;
        this.harga = harga;
        this.dekripsi = deskripsi;
    }

    public String getDekripsi() {
        return dekripsi;
    }

    public void setDekripsi(String dekripsi) {
        this.dekripsi = dekripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }


}
